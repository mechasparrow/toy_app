class CreateEyes < ActiveRecord::Migration
  def change
    create_table :eyes do |t|
      t.string :color
      t.integer :size
      t.integer :dogs_id
      t.timestamps null: false
    end
  end
end

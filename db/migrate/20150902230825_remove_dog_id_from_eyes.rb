class RemoveDogIdFromEyes < ActiveRecord::Migration
  def change
    remove_column :eyes, :dogs_ids
  end
end

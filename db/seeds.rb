# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

d1 = Dog.create(name: "happy dog", color: "red")
d2 = Dog.create(name: "buddy",color: "blue")

e1 = Eye.create(color: "green", size: 10, dog_id: d1.id)
e2 = Eye.create(color: "red",size: 5, dog_id: d1.id)
